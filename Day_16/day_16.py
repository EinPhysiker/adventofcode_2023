
class Map():

    left = (-1, 0)
    right = (1, 0)
    up = (0, -1)
    down = (0, 1)

    horizontal = [ left, right ]
    vertical = [ up, down ]

    def __init__(self, filename):

        self.map = []
        with open(filename) as f:
            while line := f.readline().strip():
                row = []
                for c in line:
                    row.append(Cell(c))
                self.map.append(row)

        self.height = len(self.map)
        self.width = len(self.map[0])

    def __getitem__(self, key):
        if self.isInside(key):
            return self.map[key[1]][key[0]]
        return None

    def isInside(self, key):
        if key[1]>=0 and key[1]<self.height and key[0]>=0 and key[0]<self.width:
            return True
        return False

    def deEnergize(self):
        for row in self.map:
            for cell in row:
                cell.energized = []

    def countEnergized(self):
        n = 0
        for row in self.map:
            for cell in row:
                if cell.energized:
                    n += 1
        return n

    def printMap(self):
        for l in self.map:
            print("".join([str(c) for c in l]))


    def processBeam(self, pos, direction):
        beams = [ [ pos, direction ] ]

        while beams:
            beam = beams.pop()
            cell = self[beam[0]]
            if not cell:
                continue
            new_beams = cell.process(beam[0], beam[1])
            beams += new_beams

        return self.countEnergized()

class Cell():

    def __init__(self, char):
        self.energized = []
        self.char = char
        self.action = self.empty
        match char:
            case "-":
                self.action = self.splitHorizontal
            case "|":
                self.action = self.splitVertical
            case "/":
                self.action = self.mirrorLeftDown
            case "\\":
                self.action = self.mirrorLeftUp
        pass

    def __repr__(self):
        return self.char

    def process(self, pos, direction):
        if direction in self.energized:
            return []
        self.energized.append(direction)
        self.char = "#"
        return self.action(pos, direction)

    def empty(self, pos, direction):
        return [ [self.step(pos, direction), direction] ]

    def splitHorizontal(self, pos, direction):
        if direction in Map.horizontal:
            return self.empty(pos, direction)
        else:
            return [
                [ self.step(pos, Map.left), Map.left ],
                [ self.step(pos, Map.right), Map.right ]
            ]
    
    def splitVertical(self, pos, direction):
        if direction in Map.vertical:
            return self.empty(pos, direction)
        else:
            return [
                [ self.step(pos, Map.up), Map.up ],
                [ self.step(pos, Map.down), Map.down ]
            ]

    def mirrorLeftDown(self, pos, direction):
        match direction:
            case Map.left:
                return [ [self.step(pos, Map.down), Map.down] ]
            case Map.right:
                return [ [self.step(pos, Map.up), Map.up] ]
            case Map.up:
                return [ [self.step(pos, Map.right), Map.right] ]
            case Map.down:
                return [ [self.step(pos, Map.left), Map.left] ]
        print("Impossible State")
        return []

    def mirrorLeftUp(self, pos, direction):
        match direction:
            case Map.left:
                return [ [self.step(pos, Map.up), Map.up] ]
            case Map.right:
                return [ [self.step(pos, Map.down), Map.down] ]
            case Map.up:
                return [ [self.step(pos, Map.left), Map.left] ]
            case Map.down:
                return [ [self.step(pos, Map.right), Map.right] ]
        print("Impossible State")
        return []

    def step(self, pos, direction):
        return (pos[0] + direction[0], pos[1] + direction[1])
    
def star_1():

    map = Map("Day_16/day_16.txt")

    n = map.processBeam( (0,0), Map.right )

    print(f"Star 1: {n}")

    
def star_2():
    map = Map("Day_16/day_16.txt")

    max = 0

    for x in range(map.width):
        map.deEnergize()
        n = map.processBeam( (x, 0), Map.down )
        if n > max:
            max = n
        map.deEnergize()
        n = map.processBeam( (x, map.height-1), Map.up )
        if n > max:
            max = n
    for y in range(map.height):
        map.deEnergize()
        n = map.processBeam( (0, y), Map.right )
        if n > max:
            max = n
        map.deEnergize()
        n = map.processBeam( (map.width-1, y), Map.left )
        if n > max:
            max = n

    print(f"Star 2: {max}")


star_1()
star_2()
