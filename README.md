# Advent of Code 2022

* All tasks are available at https://adventofcode.com/2023
* All tasks are solved in Python. Maybe sometimes other languages are used in addition.
* I also tried to keep the use of very specialized packages at a minimum.
* Code is normally pretty messy - at the end there is limited time per day 
