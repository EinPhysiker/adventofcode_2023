import re

class Box:

    def __init__(self):
        self.lenses = []
        self.focal = []

    def setLense(self, lense, focal):
        if lense in self.lenses:
            self.focal[self.lenses.index(lense)] = focal
        else:
            self.lenses.append(lense)
            self.focal.append(focal)

    def removeLense(self, lense):
        if lense in self.lenses:
            index = self.lenses.index(lense)
            del self.lenses[index]
            del self.focal[index]

    def getPower(self):
        power = 0
        for i,p in enumerate(self.focal):
            power += (i+1) * p 
        return power

def hash(s):
    val = 0
    for c in s:
        val += ord(c)
        val *= 17
        val = val % 256
    return val

def star_1():

    total = 0
    with open("Day_15/day_15.txt") as f:
        line = f.readline().strip()
            
    for s in line.split(","):
        total += hash(s)

    print(f"Star 1: {total}")

def star_2():

    with open("Day_15/day_15.txt") as f:
        line = f.readline().strip()

    boxes = {}
    re_label = re.compile("([a-z]+)([=-])(\d*)")
    for lense in line.split(","):
        label = re.findall(re_label, lense)[0]
        box_nr = hash(label[0])
        if box_nr not in boxes:
            boxes[box_nr] = Box()
        if label[1] == "=":
            boxes[box_nr].setLense(label[0], int(label[2]))
        elif label[1] == "-":
            boxes[box_nr].removeLense(label[0])

    total_power = 0
    for box_nr in boxes:
        total_power += (box_nr + 1) * boxes[box_nr].getPower()

    print(f"Star 2: {total_power}")

star_1()
star_2()
