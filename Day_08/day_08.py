# ok, I don't see that as general solution but as a
# one supported by interactive coding

import re
import sys

def read_input():
    re_path = "^[RL]+$"
    re_node = "^([0-9A-Z]+) = \(([0-9A-Z]+), ([0-9A-Z]+)\)"

    with open("Day_08/day_08.txt") as f:
        input = f.read()

    path = re.findall(re_path, input, flags=re.MULTILINE)[0]
    nodes = re.findall(re_node, input, flags=re.MULTILINE)

    node_dict = {}

    for (node,l,r) in nodes:
        node_dict[node] = { "L": l, "R": r }
    
    return (path, node_dict)

def star_1():

    path, node_dict = read_input()
    lp = len(path)

    pos = "AAA"
    step = 0
    while pos != "ZZZ":
        pos = node_dict[pos][path[step % lp]]
        step += 1

    print(f"Star 1: {step} steps")

def star_2():

    path, node_dict = read_input()
    lp = len(path)

    length_dict = {}
    for node in node_dict.keys():
        if node[2] == "Z":
            length_dict[node] = {}
            for i in range(lp):
                pos = node
                step = 0
                while True:
                    new_pos = node_dict[pos][path[ (step + i) % lp ]]
                    step += 1
                    if new_pos[2] == "Z" or new_pos == pos:
                        pos = new_pos
                        break
                    pos = new_pos 
                length_dict[node][i] = (pos, int(step / lp), step % lp)
        # print(f"Debug: {node}/n{length_dict}")

    pos_a = []
    for node in node_dict.keys():
        if node[2] == "A":
            pos = node
            step = 0
            while pos[2] != "Z":
                pos = node_dict[pos][path[step % lp]]
                step += 1
            pos_a.append( (pos, step) )

    # it seems (for whatever reason) all end up at
    # a Z after a full round 

    # print("Debug", pos_a)
    offsets = [ n[1] % lp for n in pos_a ]
    # print("Debug", offsets)

    if len(set(offsets)) != 1:
        print("Star 2: Assumption that all are at the same offset is wrong")
        sys.exit()
    if offsets[0] != 0:
        print("Star 2: Assumption that all are at offset 0 is wrong")
        sys.exit()

    assumption_1 = True
    assumption_2 = True
    for p in pos_a:
        next_step = length_dict[p[0]][p[1] % lp]
        if next_step[2] != 0:
            assumption_1 = False
            break
        if next_step[1] != int(p[1] / lp):
            assumption_2 = False
            break 
    if not assumption_1:
        print("Star 2: Assumption that Z to Z are full rounds is wrong")
        sys.exit()
    if not assumption_2:
        print("Star 2: Assumption that A to Z rounds = Z to Z rounds is wrong")
        sys.exit()

    rounds = [ int(p[1] / lp) for p in pos_a ]

    # ok, one should now get the least common multiple,
    # but I see by eye that for my input these are all primes 

    prod = 1
    for r in rounds:
        prod *= r

    print(f"Star 1: {prod * lp} steps")

star_1()
star_2()