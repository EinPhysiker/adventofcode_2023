
class Map():

    def __init__(self, filename):
        with open(filename) as f:
            self.map = f.read().splitlines()

        self.height = len(self.map)
        self.width = len(self.map[0])

    def __getitem__(self, key):
        if key>=0 and key<self.height:
            return self.map[key]
        return None

    def find_all_numbers(self):
        # brute force: find all number as coordinate list
        number_list = []
        for l in range(self.height):
            number = []
            for i in range(self.width):
                if self[l][i].isdigit():
                    number.append((l,i))
                elif number:
                    number_list.append(number)
                    number = []
            if number:
                number_list.append(number)
                number = []

        # check if number is valid
        valid_number_list = []
        for n in number_list:
            valid = False
            for c in n:
                if self.check_valid_digit(c):
                    valid = True
                    break
            if valid:
                valid_number_list.append(n)

        return valid_number_list

    def find_all_gears(self):
        # brute force: find all gears as coordinate list
        gear_list = []
        for l in range(self.height):
            for i in range(self.width):
                if self[l][i] == "*":
                    gear_list.append((l,i))
        return gear_list

    def check_valid_digit(self, c):
        l0 = c[0]
        s0 = c[1]

        for l in [ l0-1, l0, l0+1 ]:
            if l>=0 and l<self.height:
                for s in [ s0-1, s0, s0+1 ]:
                    if s>=0 and s<self.width:
                        if not self[l][s].isdigit() and self[l][s] != ".":
                            return True 
        return False

    # check if gear is neighbouring number
    def check_valid_gear(self, c, n):
        for cn in n:
            if abs(cn[0]-c[0])<2 and abs(cn[1]-c[1])<2:
                return True
        return False

    def get_number(self, n):
        number = ""
        for c in n:
            number += self.map[c[0]][c[1]]
        return int(number)

def star_1():
    """ 
    Problem: get all numbers that are adjacent to a non "." character

    """

    map = Map("Day_03/day_03.txt")
    valid_number_list = map.find_all_numbers()
    
    # sum up valid numbers
    sum = 0
    for n in valid_number_list:
        sum += map.get_number(n)

    print(f"Star 1: {sum}")

def star_2():
    """ 
    Problem: get all numbers that are adjacent to common "*" character
    """

    map = Map("Day_03/day_03.txt")
    valid_number_list = map.find_all_numbers()
    gear_list = map.find_all_gears()

    sum = 0
    for g in gear_list:
        numbers = []
        for n in valid_number_list:
            if map.check_valid_gear(g,n):
                numbers.append(map.get_number(n))
        if len(numbers) == 2:
            sum += numbers[0] * numbers[1]
    print(f"Star 2: {sum}")

star_1()
star_2()
