import re

class Hand:

    def __init__(self, cards, bit, mode = 1):
        self.card_values = {
            "2": 1, "3": 2, "4": 3, "5": 4, "6": 5, "7": 6, "8": 7, "9": 8, 
            "T": 9, "J": 10, "Q": 11, "K": 12, "A": 13 }
        self.cards = cards
        self.bit = bit
        if mode == 2:
            self.card_values["J"] = 0

        self.primary_value = self.calculate_primary_value(mode)
        self.secondary_value = self.calculate_secondary_value()

    def __repr__(self):
        return f"{self.cards}: {self.bit}, values: {self.primary_value}, {self.secondary_value}"

    def calculate_primary_value(self, mode):
        sets = {}
        for card in self.cards:
            # don't count joker for part 2
            if mode == 2 and card == "J":
                continue
            if card in sets:
                sets[card] += 1
            else:
                sets[card] = 1

        freqs = list(sets.values())
        # if cards = "JJJJJ" list is empty
        if not freqs:
            freqs = [0]
        # for part 2 add the number of joker to the most frequent card
        if mode == 2:
            num_joker = self.cards.count("J")
            freqs[freqs.index(max(freqs))] += num_joker

        value = 0
        if 5 in freqs:
            value = 6
        elif 4 in freqs:
            value = 5
        elif 3 in freqs and 2 in freqs:
            value = 4
        elif 3 in freqs:
            value = 3
        elif freqs.count(2) == 2:
            value = 2
        elif 2 in freqs:
            value = 1
        else:
            value = 0

        return value

    def calculate_secondary_value(self):
        value = 0
        rank = 0
        for c in reversed(self.cards):
            value += self.card_values[c] * pow(100, rank)
            rank += 1
        return value

    def __lt__(self, other):
        if self.primary_value != other.primary_value:
            return self.primary_value < other.primary_value
        return self.secondary_value < other.secondary_value


def star(mode):
    re_hand = re.compile(r"^([2-9TJQKA]+)\s+(\d+)", flags=re.MULTILINE)

    hands = []
    with open("Day_07/day_07.txt") as f:
        res = re.findall(re_hand, f.read())

    for (cards, bit) in res:
        hands.append(Hand(cards, int(bit), mode = mode))

    hands.sort()

    sum = 0
    rank = 1
    for hand in hands:
        sum += rank * hand.bit
        rank += 1

    print(f"Star {mode}: {sum}")

star(1)
star(2)
