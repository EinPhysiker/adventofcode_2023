import re
import functools

@functools.cache
def check(p, l):
    # if length list is empty, p should not countain any "#"
    if not l:
        if "#" in p:
            return 0
        return 1
    # if length p is too short, skip 
    if len(p) < sum(l) + len(l) - 1:
        return 0
    
    match p[0]:
        # if p starts with ".", start over with next character
        case ".":
            return check(p[1:], l)
        # if p starts with "#", check sequence
        case "#":
            if "." in p[:l[0]]:
                return 0
            if p[l[0]:] and p[l[0]] == "#":
                return 0
            return check(p[l[0]+1:], l[1:])
        # if p starts with "?", check both options
        case "?":
            options = 0
            # p[0] = "."
            options += check(p[1:], l)
            # p[0] = "#" 
            if "." in p[:l[0]] or (p[l[0]:] and p[l[0]] == "#"): 
                pass
            else:
                options += check(p[l[0]+1:], l[1:])
            return options
 
def stars():

    re_pattern = re.compile(r"([#\?\.]+)")
    re_pattern_split = re.compile(r"([#\?]+)")
    re_length = re.compile(r"\d+")

    input = []
    with open("Day_12/day_12.txt") as f:
        while line := f.readline().strip():
            pattern = re.findall(re_pattern, line)[0]
            length = [int(n) for n in re.findall(re_length, line)]
            input.append( [pattern, length] )

    sum = 0
    for (p, l) in input:
        p = ".".join(re.findall(re_pattern_split, p))
        sum += check(p, tuple(l))

    print(f"Star 1: {sum}")

    sum = 0
    for (p0, l0) in input:
        p = p0
        l = l0.copy()
        for i in range(4):
            p += "?" + p0
            l += l0
        p = ".".join(re.findall(re_pattern_split, p))
        s = check(p, tuple(l))
        sum += s

    print(f"Star 2: {sum}")

stars()
