import functools

@functools.cache
def north(x):
    return "O"*x.count("O") + "."*x.count(".")

@functools.cache
def south(x):
    return "."*x.count(".") + "O"*x.count("O")

@functools.cache
def rotate(x):
    y = []
    for i in range(len(x[0])):
        y.append("".join([r[i] for r in x]))
    return tuple(y)

@functools.cache
def move_north(x):
    y = []
    for l in x:
        y.append("#".join([ north(i) for i in l.split("#")]))
    return tuple(y)

@functools.cache
def move_south(x):
    y = []
    for l in x:
        y.append("#".join([ south(i) for i in l.split("#")]))
    return tuple(y)

def read_input(filename):

    with open(filename) as f:
        input = []
        while line := f.readline().strip():
            input.append(line)

    return tuple(input)

def eval_weight(input):
    weight = 0
    line = len(input)
    for x in input:
        weight += x.count("O") * line
        line -= 1
    return weight

def star_1():

    status = read_input("Day_14/day_14.txt")

    status = rotate(status)
    status = move_north(status)
    status = rotate(status)

    weight = eval_weight(status)

    print(f"Star 1: {int(weight)}")            

def star_2():

    weights = {}

    status = read_input("Day_14/day_14.txt")

    cycle = 0
    while True:

        w = eval_weight(status)
        if status not in weights:
            weights[status] = [ w, [cycle] ]
        else:
            weights[status][1].append(cycle)

        if len(weights[status][1]) == 3:
            break

        # col
        status = rotate(status)
        status = move_north(status)
        # row
        status = rotate(status)
        status = move_north(status)
        # col
        status = rotate(status)
        status = move_south(status)
        # row
        status = rotate(status)
        status = move_south(status)

        cycle += 1

    current = weights[status][1]
    diff = current[-1] - current[-2]
    base = current[-2]
    offset = (1000000000 - base) % diff

    total_weight = 0
    for w in weights.values():
        if base + offset in w[1]:
            total_weight = w[0]
            break

    print(f"Star 2: {total_weight}")

star_1()
star_2()