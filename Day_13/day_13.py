import re

def read_input(filename):
    input = []
    with open(filename) as f:
        pattern = []
        while line := f.readline():
            if line != "\n":
                pattern.append(line.strip())
            else:
                input.append(pattern)
                pattern = []
        if pattern:
            input.append(pattern)

    input_columns = []
    for p in input:
        pattern = []
        for i in range(len(p[0])):
            pattern.append("".join([r[i] for r in p]))
        input_columns.append(pattern)

    return input, input_columns

def star_1():

    input_rows, input_columns = read_input("Day_13/day_13.txt")

    result = 0
    for pr, pc in zip(input_rows, input_columns):
        for i in range(1,len(pr)):
            su = ""
            sd = ""
            for u,d in zip(reversed(pr[:i]), pr[i:]):
                su += u
                sd += d
            if su == sd:
                result += 100*i
                break
        for i in range(1,len(pc)):
            sl = ""
            sr = ""
            for l,r in zip(reversed(pc[:i]), pc[i:]):
                sl += l
                sr += r
            if sl == sr:
                result += i
                break
        
    print(f"Star 1: {result}")


def star_2():

    input_rows, input_columns = read_input("Day_13/day_13.txt")

    result = 0
    for pr, pc in zip(input_rows, input_columns):
        for i in range(1,len(pr)):
            su = ""
            sd = ""
            for u,d in zip(reversed(pr[:i]), pr[i:]):
                su += u
                sd += d
            c = sum(1 for a, b in zip(su, sd) if a != b)
            if c == 1:
                result += 100*i
                break
        for i in range(1,len(pc)):
            sl = ""
            sr = ""
            for l,r in zip(reversed(pc[:i]), pc[i:]):
                sl += l
                sr += r
            c = sum(1 for a, b in zip(sl, sr) if a != b)
            if c == 1:
                result += i
                break
        
    print(f"Star 2: {result}")




star_1()
star_2()
