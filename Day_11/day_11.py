def star(expansion = 2):

    # only save rows and columns of galaxies separately
    # do not store as map

    galaxies = []
    rows = set()
    columns = set()

    with open("Day_11/day_11.txt") as f:
        y = 0
        while line := f.readline().strip():
            for x in range(len(line)):
                if line[x] == "#":
                    galaxies.append((x,y))
                    rows.add(y)
                    columns.add(x)
            y += 1

    # "shortest" distance is an illusion if you cannot go diagonal
    # just count row difference and column difference
    # instead of taking one pair once, only count positive directions:
    # add <expansion> steps if row or column has no galaxy, otherwise 1
    sum_length = 0
    for (x1, y1) in galaxies:
        for (x2, y2) in galaxies:
            if x2>x1:
                empty = len(list(filter(lambda x: x not in columns, range(x1, x2))))
                sum_length += x2 - x1 + empty * (expansion - 1)
            if y2>y1:
                empty = len(list(filter(lambda x: x not in rows, range(y1, y2))))
                sum_length += y2 - y1 + empty * (expansion - 1)
    print(f"Expansion {expansion:9}: {sum_length}")

star(2)
star(1000000)
