import re

class Map:

    def __init__(self, from_item, to_item, mapping):
        self.from_item = from_item
        self.to_item = to_item
        self.mapping = mapping.copy()
        self.mapping.sort(key=take2nd)

    def __repr__(self):
        return(f"{self.from_item} to {self.to_item}: {self.mapping}")

    def to(self):
        return self.to_item

    def map_id(self, id):
        # to, from, range 
        for (t, f, r) in self.mapping:
            if id>=f and id<f+r:
                return t + (id - f)
        return id

    def map_range(self, r):
        lower = r[0]
        width = r[1]
        new_range = []
        for (t, f, r) in self.mapping:
            # lower is below map range
            if lower<f:
                # range is below map range
                if lower + width <= f:
                    new_range.append( [ lower, width] )
                    return new_range
                else:
                    # range is overlapping
                    new_range.append( [ lower, f - lower ] )
                    width -= f - lower
                    lower = f
            # lower is within map range
            if lower<f+r:
                # is fully within
                offset = lower - f
                if r - offset >= width:
                    new_range.append( [ t + offset, width ] )
                    return new_range
                else:
                    new_range.append( [ t + offset, r - offset ])
                    width -= r - offset
                    lower = f + r
        new_range.append( [ lower, width ] )
        return new_range

def take2nd(tuple):
        return tuple[1]

def build_maps(descriptions):
    re_map = re.compile(r"(?:(\w+)-to-(\w+) map:|((?: *\d+)+))")
    maps = {}
    for d in descriptions:
        from_item = None
        to_item = None
        mapping = []
        for (a,b,c) in re.findall(re_map,d):
            if a:
                from_item = a
            if b:
                to_item = b
            if c:
                mapping.append([ int(n) for n in c.split() ])
        maps[from_item] = Map(from_item, to_item, mapping)
    return maps

re_seeds = r"^seeds:((?: *\d+)+)"
re_maps = r"^\w+-to-\w+ map:\n(?:(?: *\d+)+\n)+"

with open("Day_05/day_05.txt") as f:
    input = f.read()
    seeds = [ int(n) for n in re.findall(re_seeds, input, flags=re.MULTILINE)[0].split() ]
    map_descriptions = re.findall(re_maps, input, flags=re.MULTILINE)
    maps = build_maps(map_descriptions)

# strategy: map individual ids through the maps
ids = seeds
stage = "seed"
while stage != "location":
    ids = [ maps[stage].map_id(n) for n in ids ]
    stage = maps[stage].to()
print(f"Star 1: {min(ids)}")

# strategy: map whole ranges, if necessary split
ranges = [ seeds[i:i+2] for i in range(0, len(seeds), 2)]
stage = "seed"
while stage != "location":
    new_ranges = []
    for r in ranges:
        ranges_mapped = maps[stage].map_range(r)
        for rm in ranges_mapped:
            new_ranges.append(rm)
    ranges = new_ranges
    stage = maps[stage].to()
ranges.sort()
print(f"Star 2: {ranges[0][0]}")


