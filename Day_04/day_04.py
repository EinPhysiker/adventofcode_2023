import re

def stars():
    """
    Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
    Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
    Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
    Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
    Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
    Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11

    1) Links Gewinnzahlen, rechts meine Zahlen
       punkte = 2^(treffer-1) wenn treffer, ansonsten 0
       Gesamtpunkte?

    2) Wenn Karte N X Treffer hat, bekommt man Karten N+1 bis N+X+1 mochmal
       Die neuen Karten werden auch ausgewertet (Nummer bleibt gleich), usw.
       Gesamtzahl der Karten am Ende  
    """

    # cards = [ (wins, my numbers), (wins, my numbers), ...]
    card = re.compile(r"Card\s+\d+:\s+((?:\d+\s+)+)\|\s+((?:\d+\s+)+)")
    with open("Day_04/day_04.txt") as f:        
        cards = re.findall(card, f.read())

    # for star 1
    points = 0
    # for star 2
    hits_array = []
    num_cards = len(cards)

    # get points for each card ans sum up (star 1)
    # fill array with number of hits (star 2), same seq as cards
    for numbers in cards:        
        winning_numbers = [int(n) for n in numbers[0].split()]
        my_numbers = [int(n) for n in numbers[1].split()]

        hits = 0
        for n in my_numbers:
            if n in winning_numbers:
                hits += 1
        hits_array.append(hits)
        if hits:
            points += pow(2, hits-1)

    # interate card copies starting with the original deck
    total_num_cards = num_cards
    process_cards = range(num_cards)
    new_cards = []
    while True:
        for i in process_cards:
            if hits_array[i] > 0:
                for j in range(i+1, i+1+hits_array[i]):
                    if j < num_cards:
                        new_cards.append(j)
        if not new_cards:
            break
        process_cards = new_cards
        total_num_cards += len(new_cards)
        new_cards = [] 

    print(f"Star 1: {points}")
    print(f"Star 2: {total_num_cards}")

stars()
