import re

# Fun with regex
#
# somehow I didn't manage to combine the 3 regex expressions
# and I still don't want went wrong
#

def read_game(line):
    """
    decode game list:
    Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
    Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
    Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
    Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
    Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
    """ 

    game = re.compile(r"^Game (\d+):")
    sets = re.compile(r"(?: \d+ (?:red|blue|green),?)+")
    colors = re.compile(r"(\d+) (red|blue|green)")

    res_game = re.findall(game, line)
    res_sets = re.findall(sets, line)

    game_number = int(res_game[0])

    game_sets = []
    for set in res_sets:
        res_colors = re.findall(colors, set)
        game_colors = []
        for color in res_colors:
            game_colors.append( [color[1], int(color[0])] )
        game_sets.append(game_colors)

    return (game_number, game_sets)

def stars():

    limits = {
        "red": 12, "green": 13, "blue": 14
    }

    sum_1 = 0
    sum_2 = 0

    with open("Day_02/day_02.txt") as file:
        for line in file.readlines():
            (game_number, game_sets) = read_game(line)

            lowest = {
                "red": 0, "green": 0, "blue": 0
            }
            possible = True

            for set in game_sets:
                for color in set:
                    if color[1] > limits[color[0]]:
                        possible = False
                    if color[1] > lowest[color[0]]:
                        lowest[color[0]] = color[1]

            if possible:
                sum_1 += game_number 

            power = lowest["red"]*lowest["green"]*lowest["blue"]
            sum_2 += power 

    print(f"Star 1: {sum_1}")
    print(f"Star 2: {sum_2}")

stars()

