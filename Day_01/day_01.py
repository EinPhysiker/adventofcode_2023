import re

realnumbers = {
    "0" : "0",
    "1" : "1",
    "2" : "2",
    "3" : "3",
    "4" : "4",
    "5" : "5",
    "6" : "6",
    "7" : "7",
    "8" : "8",
    "9" : "9",
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9"
}

def star_1():
    numbers = re.compile(r"(\d)")

    sum = 0
    with open("Day_01/day_01.txt") as file:
        for line in file.readlines():
            res = re.findall(numbers, line)
            code= res[0] + res[-1]
            sum += int(code) 

    print(f"Star 1: {sum}")

def star_2():
    numbers = re.compile(r"(one|two|three|four|five|six|seven|eight|nine|\d)")

    sum = 0
    with open("Day_01/day_01.txt") as file:
        for line in file.readlines():
            res = re.findall(numbers, line)
            code= realnumbers[res[0]] + realnumbers[res[-1]]
            sum += int(code) 

    print(f"Star 2: {sum}")

star_1()
star_2()

