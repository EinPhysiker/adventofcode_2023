import sys

class Map:

    def __init__(self, filename):
        self.tiles = []
        self.width = None
        self.length = 0
        self.start = 0

        with open(filename) as f:
            input = f.read().splitlines()
        self.width = len(input[0])
        self.length = len(input) * self.width
        mapping = {
            "|" : [ -self.width, self.width ], 
            "-" : [ -1, 1],
            "L" : [-self.width, 1],
            "J" : [-self.width, -1],
            "7" : [self.width, -1],
            "F" : [self.width, 1],
            "." : [],
            "S" : [-self.width, self.width, -1, 1]
        }

        pos = 0
        for line in input:
            for t in line:
                if t == "S":
                    self.start = pos

                n = []
                for d in mapping[t]:
                    n_pos = pos + d
                    if n_pos<0 or n_pos>self.length:
                        continue
                    n.append(n_pos)
                self.tiles.append(n)
                pos += 1

        self.clean_tiles()

        self.cleaned_input = []
        pos = 0
        for line in input:
            new_line = []
            for (d, t) in zip(line, self.tiles[pos:pos+self.width]):
                if t:
                    if d == "S":
                        dir0 = t[0] - self.start
                        dir1 = t[1] - self.start
                        for m in mapping:
                            if len(mapping[m]) != 2:
                                continue
                            if dir0 in mapping[m] and dir1 in mapping[m]:
                                d = m
                    new_line.append(d)
                else:
                    new_line.append(".")
            self.cleaned_input.append(new_line)
            pos += self.width        

    def clean_tiles(self):
        while True:
            cleaned = False
            for i in range(self.length):
                if not self.tiles[i]:
                    continue
                connections = []
                for n in self.tiles[i]:
                    if i in self.tiles[n]: 
                        connections.append(n)
                self.tiles[i] = connections
                if len(connections) != 2:
                    self.tiles[i] = []
                    cleaned = True
            if not cleaned:
                break

    def measure_loop(self):
        step = 0
        current = self.start
        next = self.tiles[current][0]
        while next != self.start:
            step += 1
            for n in self.tiles[next]:
                if n != current:
                    current = next
                    next = n 
                    break
        return step 

    def count_inner(self):

        inner = 0
        for line in self.cleaned_input:
            outer = True
            edges = ""
            for t in line:
                if t == "|":
                    outer = not outer
                if t in "JLF7":
                    edges += t
                if len(edges) == 2:
                    if edges == "F7" or edges == "LJ":
                        pass
                    else:
                        outer = not outer
                    edges = ""
                if not outer and t == ".":
                    inner += 1
        return inner

def stars():

    map = Map("Day_10/day_10.txt")

    length = map.measure_loop()
    inner = map.count_inner()

    print(f"Star 1: {int((length+1)/2)}")
    print(f"Star 2: {inner}")

stars()
