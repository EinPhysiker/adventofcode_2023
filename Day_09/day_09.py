def stars():
    with open("Day_09\day_09.txt") as f:
        lines = f.read().splitlines()

    sum1 = 0
    sum2 = 0
    for line in lines:
        seq = [ int(n) for n in line.split()]
        seq_list = [ seq ]

        while True:
            new_seq = [ seq[i+1] - seq[i] for i in range(len(seq) - 1) ]
            if len(set(new_seq)) == 1 and new_seq[0] == 0:
                break
            seq_list.append( new_seq )
            seq = new_seq

        add_val = 0
        sub_val = 0
        for seq in reversed(seq_list):
            add_val = seq[-1] + add_val
            sub_val = seq[0] - sub_val

        sum1 += add_val
        sum2 += sub_val

    print(f"Star 1: {sum1}")
    print(f"Star 2: {sum2}")

stars()
