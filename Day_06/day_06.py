# Comment
# 
# In principle, the first part looks more like a physics exercise in
# kinematics. However, the test on equality for the boundaries should be 
# done properly w/o depending on floating point precision.
#
# The second part relies on precise integer arithmetics for large numbers -
# at least I guess so, but I didn't check in other languages. It feels a bit 
# like cheating with python's arbitrary-precision integers, because there the
# solution is simply build-in. 

from math import isqrt, ceil, floor

# v = a * t
# d = v * (T - t)
# a = 1
# t_1,2 = 1/2 * (T +- sqrt(T*T - 4*d))
def num_of_options(T,d):
    # note: isqrt gives the floor of the sqrt, which is what we need
    # for both t1 and t2. This gives the correct result also for integers 
    # that are too large to have the correct answer from floor(sqrt(i)) 
    l = isqrt(T*T - 4*d)

    t1 = ceil(1/2*(T - l))
    d1 = t1*(T-t1)
    if d1 == d:
        t1 += 1
    
    t2 = floor(1/2*(T + l))
    d2 = t2*(T-t2)
    if d2 == d:
        t2 -= 1
    
    return t2 - t1 + 1

def star_1():

    with open("Day_06/day_06.txt") as f:
        race_t = [ int(n) for n in f.readline().split()[1:] ]
        race_d = [ int(n) for n in f.readline().split()[1:] ]

    product = 1
    for (T,d) in zip(race_t, race_d):
        product *= num_of_options(T,d)

    print(f"Star 1: {product}")

def star_2():

    with open("Day_06/day_06.txt") as f:
        T = int("".join(f.readline().split()[1:]))
        d = int("".join(f.readline().split()[1:]))

    print(f"Star 1: {num_of_options(T,d)}")

star_1()
star_2()